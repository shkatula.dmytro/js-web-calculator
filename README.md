# Calculator

A simple Calculator App built with HTML, CSS, and JavaScript. It also has a Dark Mode.
The first version of this calculator was created by the [zxcodes.github.io/Calculator](url)
I decided to improve it and added the following features:
- backspace function
- brackets function
- precentage function
- negative/positive value function
- click sound
- error sound
- full keyboard input support

![Calculator Preview Image](https://user-images.githubusercontent.com/44538497/169086855-bd20e6e0-3675-4db6-b086-0298005973f4.png)

* Favicon from:
<a href="https://www.flaticon.com/free-icons/calculator" title="calculator icons">Freepik - Flaticon</a>
