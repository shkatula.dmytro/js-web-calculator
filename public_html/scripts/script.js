const lightTheme = "styles/light.css";
const darkTheme = "styles/dark.css";
const sunIcon = "assets/SunIcon.svg";
const moonIcon = "assets/MoonIcon.svg";
const clickShort = "assets/audio/clickShort.mp3";
const clickLong = "assets/audio/clickLong.mp3";
const errorShort = "assets/audio/errorShort.mp3";
const themeIcon = document.getElementById("theme-icon");
const res = document.getElementById("result");
const toast = document.getElementById("toast");
const operators = ["+", "-", "*", "/"];
const digits = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
let firstPlay = false;

function calculate(value) {
  value = autocompleteBrackets(value);
  const calculatedValue = eval(value || null);
  if (isNaN(calculatedValue)) {
    res.value = "Can't divide 0 by 0";
    setTimeout(() => {
      res.value = "";
    }, 1300);
  } else {
    res.value = calculatedValue;
  }
}

function getLastNumberFromInput() {
  const matches = res.value.match(/[0-9]+$/);
  if (matches) {
    return parseInt(matches[0], 10);
  }
  return null;
}

function removeLastNumberfromInput() {
  const lastNuber = getLastNumberFromInput();
  if (Number.isInteger(lastNuber)) {
    for (let i = 0; i < lastNuber.toString().length; i++) {
      console.log("before > " + res.value);
      removeLastChar();
      console.log("after  > " + res.value);
    }
    return lastNuber;
  }
}

// Swaps the stylesheet to achieve dark mode.
function changeTheme() {
  const theme = document.getElementById("theme");
  setTimeout(() => {
    toast.innerHTML = "Calculator";
  }, 1500);
  if (theme.getAttribute("href") === lightTheme) {
    playSFX(clickLong);
    theme.setAttribute("href", darkTheme);
    themeIcon.setAttribute("src", sunIcon);
    toast.innerHTML = "Dark Mode";
  } else {
    playSFX(clickLong);
    theme.setAttribute("href", lightTheme);
    themeIcon.setAttribute("src", moonIcon);
    toast.innerHTML = "Light Mode";
  }
}

// Displays entered value on screen.
function liveScreen(enteredValue) {
  if (!res.value) {
    res.value = "";
  }

  if (operators.includes(enteredValue)) {
    addOperator(enteredValue);
  } else {
    res.value += enteredValue;
  }
}

function removeLastChar() {
  const resultInput = res.value;
  if (resultInput != "") {
    res.value = resultInput.substring(0, res.value.length - 1);
  } else {
    playSFX(errorShort);
  }
}

function toNegative() {
  if (res.value.length != 0) {
    let lastNumber = removeLastNumberfromInput();
    addBrackets();
    res.value += "-" + lastNumber;
  } else {
    return;
  }
}

function celarInput() {
  result.value = "";
}

function addOperator(operator) {
  const lastChar = res.value.at(-1);
  if (
    operators.includes(lastChar) ||
    lastChar === undefined ||
    lastChar === "("
  ) {
    playSFX(errorShort);
  } else {
    res.value += operator;
  }
}

function addBrackets() {
  const lastChar = res.value.at(-1);
  const bracketOpen = res.value.count("[(]");
  const bracketClose = res.value.count("[)]");
  if (lastChar === "(" || lastChar === undefined) {
    res.value += "(";
  } else if (lastChar === ")") {
    if (bracketOpen < bracketClose) {
      res.value += ")";
    } else if (bracketOpen > bracketClose) {
      res.value += ")";
    } else {
      res.value += "*(";
    }
  } else if (digits.includes(lastChar) && bracketOpen === bracketClose) {
    console.log("This!");
    res.value += "(";
  } else if (operators.includes(lastChar)) {
    res.value += "(";
  } else {
    res.value += ")";
  }
}

function autocompleteBrackets(value) {
  const bracketOpen = value.count("[(]");
  const bracketClose = value.count("[)]");
  const diff = bracketOpen - bracketClose;
  if (diff === 0) {
  } else {
    for (var i = 0; i < diff; i++) {
      value += ")";
    }
  }
  return value;
}