//Get the number of occurrences in a string
String.prototype.count = function (inputLog) {
  return this.length - this.replace(new RegExp(inputLog, "g"), "").length;
};

//Play sound effects 
function playSFX(url) {
  if (!firstPlay) {
    new Audio(url).play();
  } else {
    firstPlay = true;
  }
}


//adding event handler on the document to handle keyboard inputs
document.addEventListener("keydown", keyboardInputHandler);

//function to handle keyboard inputs
function keyboardInputHandler(e) {
  playSFX(clickShort);

  // to fix the default behavior of browser,
  // enter and backspace were causing undesired behavior when some key was already in focus.
  e.preventDefault();
  //grabbing the liveScreen

  //numbers
  if (e.key === "0") {
    res.value += "0";
  } else if (e.key === "1") {
    res.value += "1";
  } else if (e.key === "2") {
    res.value += "2";
  } else if (e.key === "3") {
    res.value += "3";
  } else if (e.key === "4") {
    res.value += "4";
  } else if (e.key === "5") {
    res.value += "5";
  } else if (e.key === "6") {
    res.value += "6";
  } else if (e.key === "7") {
    res.value += "7";
  } else if (e.key === "7") {
    res.value += "7";
  } else if (e.key === "8") {
    res.value += "8";
  } else if (e.key === "9") {
    res.value += "9";
  }

  //unwantedChars
  if (e.key === "+") {
    addOperator("+");
  } else if (e.key === "-") {
    addOperator("-");
  } else if (e.key === "*") {
    addOperator("*");
  } else if (e.key === "/") {
    addOperator("/");
  }

  //brackets
  if (e.key === "(") {
    addBrackets();
  } else if (e.key === ")") {
    // res.value += ")";
    addBrackets();
  }

  //percentage key
  if (e.key === "%") {
    //res.value += "%";
    console.log(res.value);
    convertLastNumberToDecimal();
  }

  //clear key
  if (e.key === "c") {
    celarInput();
  }

  //decimal key
  if (e.key === ".") {
    res.value += ".";
  }

  //press enter to see result
  if (e.key === "Enter" || e.key === "=") {
    calculate(result.value);
  }

  //backspace key for removing the last input
  if (e.key === "Backspace") {
    removeLastChar();
  }
}
